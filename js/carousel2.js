var current_item = 0;
$(document).ready(function() {
	//$('.carousel2-item').load('card3.html');
	//console.log($(".product").length)
	var numImages = $(".carousel2-item").length;
	$('.left-arrow').on('click',function() {
		//console.log(Math.round($( window ).width()/$(".card3").width()))
		if (current_item > 0) {
			current_item = current_item - 1;
		} else {
			current_item = numImages - Math.round($(".carousel2-cont-0").width()/$(".card3").width())+1;
		}
		//console.log("current_item"+current_item)
		$(".carousel2-cont").animate({"left": -($('#carousel2-item-'+current_item).position().left)}, 500);
		return false;
	});

	$('.right-arrow').on('click', function() {
		//console.log(Math.round($( window ).width()/$(".card3").width()))
		if (numImages > current_item + Math.round($(".carousel2-cont-0").width()/$(".card3").width())-1) {
			current_item = current_item + 1;
		} else {
			current_item = 0;
		}
		//console.log("current_item"+current_item)
		$(".carousel2-cont").animate({"left": -($('#carousel2-item-'+current_item).position().left)}, 500);
		return false;
	}); 

	$('.left-arrow').on('hover', function() {
		$(this).css('opacity','0.5');
	}, function() {
		$(this).css('opacity','1');
	});

	$('.right-arrow').on('hover', function() {
		$(this).css('opacity','0.5');
	}, function() {
		$(this).css('opacity','1');
	});
});