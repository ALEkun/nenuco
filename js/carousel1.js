$(document).ready(function(){
    $('.productScroller').slick({
      infinite: true,
      dots: true,
      lazyLoad: "ondemand",
      centerPadding: '15px',
      slidesToShow: 5,
      nextArrow: '<button type="button" data-role="none" class="control-arrows slick-next slick-arrow" aria-label="Next" role="button">Next</button>',
      prevArrow: '<button type="button" data-role="none" class="control-arrows slick-prev slick-arrow" aria-label="Previous" role="button">Previous</button>',
      slidesToScroll: 5,
      responsive: [{
              breakpoint: 1024,
              settings: {
                  slidesToShow: 3,
                  slidesToScroll: 3,
                  infinite: true,
                  dots: true, 
                arrows: false
              }
          }, {
              breakpoint: 600,
              settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2,
                arrows: false
              }
          }, {
              breakpoint: 480,
              settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1
              }
          }]
    });
  });